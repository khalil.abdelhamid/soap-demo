import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EtudiantDAO {

    private Connection cnx = JDBConnection.getConnection();

    public Integer save(Etudiant obj) {
        try {
            PreparedStatement ps = cnx.prepareStatement("INSERT INTO ETUDIANT (nom, " +
                            "prenom) VALUE (? ?)", Statement.RETURN_GENERATED_KEYS);

            ps.setString(1, obj.getNom());
            ps.setString(1, obj.getPrenom());
            if (ps.executeUpdate() == 0)
                return 0;
            ResultSet result = ps.getGeneratedKeys();
            if (result.next())
                return result.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public Etudiant getById(Integer cne) {
        try {
            PreparedStatement ps = cnx.prepareStatement("SELECT * FROM etudiant WHERE  " + "cne = ?");
            ps.setInt(1, cne);
            ResultSet result = ps.executeQuery();
            if (result.next()) {
                Etudiant etudiant = new Etudiant();
                etudiant.setCne(result.getInt("cne"));
                etudiant.setNom(result.getString("nom"));
                etudiant.setPrenom(result.getString("prenom"));
                return etudiant;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Etudiant> getAll() {
        try {
            List<Etudiant> etapes = new ArrayList<>(10);
            Statement stmt = cnx.createStatement();
            ResultSet result = stmt.executeQuery("SELECT * FROM etudiant");
            while (result.next()) {
                Etudiant etudiant = new Etudiant();
                etudiant.setCne(result.getInt("cne"));
                etudiant.setNom(result.getString("nom"));
                etudiant.setPrenom(result.getString("prenom"));
                etapes.add(etudiant);
                return etapes;
            }
            return etapes;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Boolean update(Etudiant obj) {
        try {
            PreparedStatement ps = cnx.prepareStatement("UPDATE etudiant SET nom = ? " +
                    "and prenom = ? where cne = ?");
            ps.setString(1, obj.getNom());
            ps.setString(2, obj.getPrenom());
            ps.setInt(3, obj.getCne());
            return ps.executeUpdate() != 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public Boolean delete(Etudiant obj) {
        try {
            PreparedStatement ps = cnx.prepareStatement("DELETE FROM etudiant WHERE cne = ?");
            ps.setInt(1, obj.getCne());
            return ps.executeUpdate() != 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
